FROM nvidia/cuda:10.0-cudnn7-devel-ubuntu18.04

RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get -y install python3-dev python3-pip && \
    apt-get -y install build-essential cmake unzip pkg-config && \
    apt-get -y install libjpeg-dev libpng-dev libtiff-dev && \
    apt-get -y install libavcodec-dev libavformat-dev libswscale-dev && \
    apt-get -y install libv4l-dev libxvidcore-dev libx264-dev && \
    apt-get -y install libgtk-3-dev && \
    apt-get -y install libatlas-base-dev gfortran && \
    apt-get -y install python3-dev

COPY requirements.txt requirements.txt

RUN pip3 install -r requirements.txt

COPY ./opencv /opencv
COPY ./opencv_contrib /opencv_contrib

RUN cd opencv && mkdir build

WORKDIR /opencv/build

RUN cmake -D CMAKE_BUILD_TYPE=RELEASE \
    -D CMAKE_INSTALL_PREFIX=/usr/local \
    -D INSTALL_PYTHON_EXAMPLES=ON \
    -D INSTALL_C_EXAMPLES=OFF \
    -D OPENCV_ENABLE_NONFREE=ON \
    -D WITH_CUDA=ON \
    -D WITH_CUDNN=ON \
    -D OPENCV_DNN_CUDA=ON \
    -D ENABLE_FAST_MATH=1 \
    -D CUDA_FAST_MATH=1 \
    -D CUDA_ARCH_BIN=6.1 \
    -D WITH_CUBLAS=1 \
    -D OPENCV_EXTRA_MODULES_PATH=/opencv_contrib/modules \
    -D HAVE_opencv_python3=ON \
    -D PYTHON_EXECUTABLE=/usr/bin/python3 \
    -D BUILD_EXAMPLES=ON ..

RUN make -j8

RUN make install && ldconfig


